Dado('que ficho al jugador {string} de nacionalidad {string}') do |nombre, nacionalidad|
  body = {
    'nombre': nombre,
    'nacionalidad': nacionalidad,
    'posicion': 'defensor',
    'valor_de_mercado': 100,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('el jugador {string} de nacionalidad {string}') do |nombre, nacionalidad|
  @nombre = nombre
  @nacionalidad = nacionalidad
end

Dado('que es posicion {string}') do |posicion|
  @posicion = posicion
end

Dado('su valor de mercado es {int}') do |valor_mercado|
  @valor_de_mercado = valor_mercado
end

Dado('su potencialidad es {int}') do |potencialidad|
  @potencialidad = potencialidad
end

Cuando('se ficha al jugador') do
  body = {
    'nombre': @nombre,
    'nacionalidad': @nacionalidad,
    'posicion': @posicion,
    'valor_de_mercado': @valor_de_mercado,
    'potencialidad': @potencialidad
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Cuando('ficho al jugador {string} con valor {int}') do |nombre, valor|
  body = {
    'nombre': nombre,
    'nacionalidad': 'argentino',
    'posicion': 'delantero',
    'valor_de_mercado': valor,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('obtengo un numero de fichaje') do
  expect(last_response.status).to be == 201
  @id_equipo = JSON.parse(last_response.body)['id']
  expect(@id_equipo).not_to be_nil
end

Cuando('ficho al jugador {string} de nacionalidad {string}') do |nombre, nacionalidad|
  body = {
    'nombre': nombre,
    'nacionalidad': nacionalidad,
    'posicion': 'delantero',
    'valor_de_mercado': 10,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('obtengo un error de fichaje') do
  expect(last_response.status).to be == 400
end

Dado('que ficho al arquero {string}') do |nombre|
  body = {
    'nombre': nombre,
    'nacionalidad': 'argentino',
    'posicion': 'arquero',
    'valor_de_mercado': 10,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Cuando('ficho al arquero {string}') do |nombre|
  body = {
    'nombre': nombre,
    'nacionalidad': 'argentino',
    'posicion': 'arquero',
    'valor_de_mercado': 10,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que la fecha actual es {string}') do |fecha|
  post '/fecha_fija', { :fecha => fecha }.to_json, 'CONTENT_TYPE' => 'application/json'
end

Cuando('ficho al jugador {string}') do |nombre|
  body = {
    'nombre': nombre,
    'nacionalidad': 'argentino',
    'posicion': 'arquero',
    'valor_de_mercado': 10,
    'potencialidad': 1
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('obtengo un error por fin de semana') do
  expect(last_response.status).to be == 400
  error = JSON.parse(last_response.body)['error']
  expect(error).to eq 'error_por_fin_de_semana'
end
