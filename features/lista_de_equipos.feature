#language: es
Característica: Lista de equipos

  Antecedentes:
    Dado que existe el equipo "River" con presupuesto 100
    Dado que existe el equipo "Boca" con presupuesto 100
    Y que ficho al jugador "Juan Chilo" con valor 10
    Dado que existe el equipo "Racing" con presupuesto 1000
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 30 delanteros con potencial 5

  @wip
  Escenario: l1 - Consulta de listado de equipos
    Cuando consulto la lista de equipos
    Entonces entonces tengo 3 equipos

  @wip
  Escenario: l2 - Consulta de equipo con estado incompleto
    Cuando consulto la lista de equipos
    Entonces el equipo "River" esta "INCOMPLETO"

  @wip
  Escenario: l3 - Consulta de equipo con estado incompleto
    Cuando consulto la lista de equipos
    Entonces el equipo "Boca" esta "INCOMPLETO"

  @wip
  Escenario: l4 - Consulta de equipo con estado completo
    Cuando consulto la lista de equipos
    Entonces el equipo "Racing" esta "COMPLETO"

  @wip
  Escenario: l5 - Consulta de presupuesto ejecutado cuando hay impuestos impuestos j2
    Dado que existe el equipo "Independiente" con presupuesto 1000
    Y que ficho al jugador "Juan Perez" de nacionalidad "chilena" con valor 10
    Y que ficho al jugador "Juan Gomez" de nacionalidad "española" con valor 10
    Cuando consulto sus detalles
    Entonces el presupuesto ejecutado del equipo es 26

  @wip
  Escenario: l6 - Consulta de presupuesto ejecutado cuando hay impuestos impuestos j1
    Dado que existe el equipo "Independiente" con presupuesto 1000
    Y que ficho al jugador "Juan Chilote" de nacionalidad "chilena" con valor 100
    Cuando consulto sus detalles
    Entonces el presupuesto ejecutado del equipo es 130
