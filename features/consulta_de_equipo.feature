#language: es
Característica: Consulta de equipo
  
  @wip
  Escenario: c1 - Consulta de equipo con 2 arqueros y 30 delanteros sin potencial
    Dado que existe el equipo "Los Malos" con presupuesto 3000
    Y que tiene 2 arqueros con potencial 0
    Y que tiene 30 delanteros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 30
    Y poder defensivo es 2

  @wip
  Escenario: c2 - Consulta de equipo con 2 arqueros y 30 delanteros con potencial
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 4
    Y que tiene 12 mediocampistas con potencial 3
    Y que tiene 10 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 112
    Y poder defensivo es 116      

  @wip
  Escenario: dorne 1 - Consulta de equipo con 2 arqueros, 2 defensores y 28 delanteros con potencial 2
    Dado que existe el equipo "Los kamikaze"
    Y que tiene 2 arqueros con potencial 2
    Y que tiene 2 defensores con potencial 2
    Y que tiene 28 delanteros con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 92
    Y poder defensivo es 68

  @wip
  Escenario: dorne 2 - Consulta de equipo con 2 arqueros, 10 defensores y 20 mediocampistas con distinto potencial
    Dado que existe el equipo "Los defensivos"
    Y que tiene 2 arqueros con potencial 1
    Y que tiene 10 defensores con potencial 5
    Y que tiene 20 mediocampistas con potencial 3
    Cuando consulto sus detalles
    Entonces poder ofensivo es 122
    Y poder defensivo es 134

  @wip
  Escenario: dorne 3 - Consulta de equipo con 2 arqueros, 22 mediocampistas y 8 delanteros con distinto potencial
    Dado que existe el equipo "Los arriesgados"
    Y que tiene 2 arqueros con potencial 1
    Y que tiene 22 mediocampistas con potencial 4
    Y que tiene 8 delanteros con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 125
    Y poder defensivo es 119

  @wip
  Escenario: altojardin1 - Consulta de equipo con 2 arqueros, 4 defensores, 3 mediocampistas y 3 delanteros
    Dado que existe el equipo "River"
    Y que tiene 2 arqueros con potencial 3
    Y que tiene 4 defensores con potencial 3
    Y que tiene 3 mediocampistas con potencial 3
    Y que tiene 3 delanteros con potencial 3
    Cuando consulto sus detalles
    Entonces poder ofensivo es 40
    Y poder defensivo es 43

  @wip
  Escenario: altojardin2 - Consulta de equipo con 2 arqueros, 5 defensores, 4 mediocampistas y 2 delanteros
    Dado que existe el equipo "Boca"
    Y que tiene 2 arqueros con potencial 2
    Y que tiene 5 defensores con potencial 4
    Y que tiene 4 mediocampistas con potencial 2
    Y que tiene 2 delanteros con potencial 3
    Cuando consulto sus detalles
    Entonces poder ofensivo es 42
    Y poder defensivo es 47

  @wip
  Escenario: altojardin3 - Consulta de equipo con 2 arqueros y 1 mediocampista
    Dado que existe el equipo "Racing"
    Y que tiene 2 arqueros con potencial 4
    Y que tiene 1 mediocampistas con potencial 3
    Cuando consulto sus detalles
    Entonces poder ofensivo es 11
    Y poder defensivo es 13
    
  @wip
  Escenario: elmuro1 - Consulta de equipo con 2 arqueros con potencial, 10 delanteros sin potencial y 20 mediocamoistas con potencial
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 0
    Y que tiene 20 mediocampistas con potencial 4
    Y que tiene 0 defensores con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 108
    Y poder defensivo es 102

  @wip
  Escenario: elmuro2 - Consulta de equipo con 3 arqueros con potencial
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 4
    Y poder defensivo es 6

  @wip
  Escenario: elmuro3 - Consulta de equipo con 5 delanteros con potencial 5 
    Dado que existe el equipo "Los Buenos"
    Y que tiene 5 delanteros con potencial 5
    Cuando consulto sus detalles
    Entonces poder ofensivo es 30
    Y poder defensivo es 25

  @wip
  Escenario: rocadragon1 - Consulta de equipo con 1 arquero, 20 defensores y 11 mediocampistas con potencial
    Dado que existe el equipo "Rocadragon"
    Y que tiene 1 arqueros con potencial 1
    Y que tiene 11 mediocampistas con potencial 5
    Y que tiene 20 defensores con potencial 0    
    Cuando consulto sus detalles
    Entonces poder ofensivo es 61
    Y poder defensivo es 82

  @wip
  Escenario: rocadragon2 - Consulta de equipo con 2 arquero, 5 delantero, 15 defensores y 10 mediocampistas con potencial
    Dado que existe el equipo "Rocadragon"
    Y que tiene 2 arqueros con potencial 3
    Y que tiene 5 delanteros con potencial 1
    Y que tiene 10 mediocampistas con potencial 0
    Y que tiene 15 defensores con potencial 0    
    Cuando consulto sus detalles
    Entonces poder ofensivo es 21
    Y poder defensivo es 33  

  @wip
  Escenario: rocadragon3 - Consulta de equipo con 2 arqueros, 10 delantero, 10 defensores y 10 mediocampistas con potencial
    Dado que existe el equipo "Rocadragon"
    Y que tiene 2 arqueros con potencial 2
    Y que tiene 10 delanteros con potencial 2
    Y que tiene 10 mediocampistas con potencial 2
    Y que tiene 10 defensores con potencial 2    
    Cuando consulto sus detalles
    Entonces poder ofensivo es 79
    Y poder defensivo es 81

  @wip
  Escenario: invernalia1 - Consulta de equipo con 2 arqueros y 30 defensores sin potencial
    Dado que existe el equipo "Los No Ofensivos"
    Y que tiene 2 arqueros con potencial 0
    Y que tiene 30 defensores con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 0
    Y poder defensivo es 32      

@wip
  Escenario: invernalia2 - Consulta de equipo con 2 arqueros y 30 delanteros con distinto potencial
    Dado que existe el equipo "Los Delanteros"
    Y que tiene 2 arqueros con potencial 0
    Y que tiene 10 delanteros con potencial 1
    Y que tiene 10 delanteros con potencial 2
    Y que tiene 10 delanteros con potencial 3
    Cuando consulto sus detalles
    Entonces poder ofensivo es 90
    Y poder defensivo es 62   

@wip
  Escenario: invernalia3 - Consulta de equipo con 2 arqueros y 30 jugadores con distintos potenciales
    Dado que existe el equipo "Los Mixtos"
    Y que tiene 1 arqueros con potencial 0
    Y que tiene 1 arqueros con potencial 5
    Y que tiene 4 delanteros con potencial 2
    Y que tiene 4 delanteros con potencial 3
    Y que tiene 6 mediocampistas con potencial 1
    Y que tiene 6 mediocampistas con potencial 4
    Y que tiene 5 defensores con potencial 3
    Y que tiene 5 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 94
    Y poder defensivo es 98      

  @wip
  Escenario: elvalle1 - Consulta de equipo con 10 defensores sin potencial
    Dado que existe el equipo "Los Defensores"
    Y que tiene 10 defensores con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 0
    Y poder defensivo es 10

  @wip
  Escenario: elvalle2 - Consulta de equipo con 10 mediocampistas sin potencial
    Dado que existe el equipo "Los Mediocampistas"
    Y que tiene 10 mediocampistas con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 5
    Y poder defensivo es 5
  
  @wip
  Escenario: elvalle3 - Consulta de equipo con 10 delanteros sin potencial
    Dado que existe el equipo "Los Delanteros"
    Y que tiene 10 delanteros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 10
    Y poder defensivo es 0

  @wip
  Escenario: elvalle4 - Consulta de equipo con 2 arqueros y 10 en cada posicion
    Dado que existe el equipo "El Equilibrio"
    Y que tiene 10 delanteros con potencial 0
    Y que tiene 10 mediocampistas con potencial 0
    Y que tiene 10 defensores con potencial 0
    Y que tiene 2 arqueros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 15
    Y poder defensivo es 17

  @wip
  Escenario: antigua1 - Consulta de equipo con 2 arqueros, 18 delanteros, 6 mediocampistas y 6 defensores con potencial
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 18 delanteros con potencial 0
    Y que tiene 6 mediocampistas con potencial 4
    Y que tiene 6 defensores con potencial 3
    Cuando consulto sus detalles
    Entonces poder ofensivo es 73
    Y poder defensivo es 63

  @wip
  Escenario: antigua2 - Consulta de equipo con 2 arqueros, 4 delanteros, 12 mediocampistas y 8 defensores con potencial
    Dado que existe el equipo "Los Maso"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 4 delanteros con potencial 2
    Y que tiene 12 mediocampistas con potencial 0
    Y que tiene 8 defensores con potencial 1
    Cuando consulto sus detalles
    Entonces poder ofensivo es 36
    Y poder defensivo es 42

  @wip
  Escenario: antigua3 - Consulta de equipo con 2 arqueros, 10 delanteros y 7 defensores con potencial
    Dado que existe el equipo "Los Antigua"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 10 delanteros con potencial 4
    Y que tiene 7 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 74 
    Y poder defensivo es 73

  @wip 
  Escenario: pentos1 - Consulta de equipo con 2 arqueros, 20 delanteros y 10 defensores con potencial
    Dado que existe el equipo "Los Buenos"     
    Y que tiene 2 arqueros con potencial 5     
    Y que tiene 20 delanteros con potencial 4     
    Y que tiene 10 defensores con potencial 2     
    Cuando consulto sus detalles     
    Entonces poder ofensivo es 130
    Y poder defensivo es 122

  @wip   
  Escenario: pentos2 - Consulta de equipo con 2 arqueros     
    Dado que existe el equipo "Los Buenos"     
    Y que tiene 2 arqueros con potencial 5     
    Cuando consulto sus detalles     
    Entonces poder ofensivo es 10
    Y poder defensivo es 12
     
  @wip   
  Escenario: pentos3 - Consulta de equipo con 2 arqueros y 30 defensores     
    Dado que existe el equipo "Los Buenos"     
    Y que tiene 2 arqueros con potencial 3     
    Y que tiene 30 defensores con potencial 2     
    Cuando consulto sus detalles     
    Entonces poder ofensivo es 66     
    Y poder defensivo es 98

  @wip
  Escenario: volantis1 - Consulta de equipo con 2 arqueros, 8 delanteros, 12 mediocampistas y 10 defensores con potencial
    Dado que existe el equipo "Volantis"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 5
    Y que tiene 12 mediocampistas con potencial 5
    Y que tiene 10 defensores con potencial 5
    Cuando consulto sus detalles
    Entonces poder ofensivo es 174
    Y poder defensivo es 178

  @wip
  Escenario: volantis2 - Consulta de equipo con 10 mediocampistas y 10 delanteros sin potencial
    Dado que existe el equipo "Bokita"
    Y que tiene 10 delanteros con potencial 0
    Y que tiene 10 mediocampistas con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 15
    Y poder defensivo es 5

  @wip
  Escenario: volantis3 - Consulta de equipo con 2 arqueros, 25 defensores y 5 delanteros con potencial
    Dado que existe el equipo "Sacachispas"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 5 delanteros con potencial 4
    Y que tiene 25 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 85
    Y poder defensivo es 107
